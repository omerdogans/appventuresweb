"use strict";

$(function() {
    const MAP_BACKGROUND_COLOUR = "#000000";
    const INACTIVE_MAP_COLOUR = "#c4fb6d";
    const ACTIVE_MAP_COLOURS = [
        "#00C4F0",
        "#00BCE5",
        "#00B3DB",
        "#00ABD1",
        "#00A3C7"
    ];

    loadData(function() {
        initialiseMap();
    });

    function initialiseMap() {
        _map = new jvm.Map({
            map: "world_merc",
            container: $("#map"),
            backgroundColor: "#000000",
            zoomMin: 0.9,
            focusOn: {
                x: 0.5,
                y: 0.5,
                scale: 0.95
            },
            series: {
                regions: [{
                    attribute: "fill"
                }]
            },
        });

        // Set map colours
        _map.series.regions[0].setValues(getMapColours());
    }

    function animateWithDeceleration(min, max, tickCallback, doneCallback) {
        let delayMs = 20;
        let fn = function(val) {
            tickCallback(val);
            if (val < max) {
                delayMs *= (val < (max - 10)) ? 1 : 1.3;
                setTimeout(function() {
                    fn(val + 1)
                }, delayMs);
            } else {
                doneCallback();
            }
        };
        fn(min);
    }

});